package com.varun.voiceimagesearch.utils;

import com.varun.voiceimagesearch.model.Photo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class ImageUtilsTest {


    @Test
    public void testPhotoUrlCreated() {

        Photo photo = new Photo();
        photo.setFarm(1);
        photo.setServer("server");
        photo.setId("id");
        photo.setSecret("secret");
        Assert.assertEquals("http://farm1.static.flickr.com/server/id_secret.jpg", ImageUtils.getPhotoUrl(photo));
    }

    @Test
    public void testEmptyPhotoUrlCreated() {

        Assert.assertEquals("", ImageUtils.getPhotoUrl(null));
    }

    @Test
    public void testImageRequestManagerInstance() {

        Assert.assertNotNull(ImageUtils.getImageRequestManagerInstance());
    }

}
