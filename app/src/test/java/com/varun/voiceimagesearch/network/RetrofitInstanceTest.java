package com.varun.voiceimagesearch.network;

import org.junit.Assert;
import org.junit.Test;

import retrofit2.Retrofit;

public class RetrofitInstanceTest {

    @Test
    public void testInstanceCreated() {

        Assert.assertNotNull(RetrofitInstance.getRetrofitInstance());
    }

    @Test
    public void testSingleton() {

        Retrofit instance = RetrofitInstance.getRetrofitInstance();

        Assert.assertEquals(instance, RetrofitInstance.getRetrofitInstance());

    }
}
