package com.varun.voiceimagesearch.core;

import com.varun.voiceimagesearch.model.Photo;
import com.varun.voiceimagesearch.model.Photos;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {

    private MainActivity activity;


    @Mock
    private GetPhotosServiceImpl getPhotosService;

    @Captor
    private ArgumentCaptor<MainFactory.GetPhotosInteractor.OnFinishedListener> cb;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        ActivityController<MainActivity> controller = Robolectric.buildActivity(MainActivity.class);
        activity = controller.get();
        activity.setGetPhotosService(getPhotosService);
        controller.create();
        activity.fetchDataFromTheServer("test", true);
    }

    @Test
    public void testPhotoListPopulated() {

        Mockito.verify(activity.getGetPhotosService()).getPhotosList(Mockito.anyString(), Mockito.anyInt(), cb.capture());

        Photo photo = new Photo();
        Photos photos = new Photos();
        photos.getPhotos().add(photo);
        cb.getValue().onFinished(photos);
        Assert.assertEquals(activity.getPhotosAdapter().getItemCount(), 2);
    }

    @Test
    public void testPhotoListPopulatedEmpty() {

        Mockito.verify(activity.getGetPhotosService()).getPhotosList(Mockito.anyString(), Mockito.anyInt(), cb.capture());

        Photos photos = new Photos();
        cb.getValue().onFinished(photos);
        Assert.assertTrue(ShadowToast.getTextOfLatestToast().contains("No data returned"));
        Assert.assertEquals(activity.getPhotosAdapter().getItemCount(), 0);
    }

    @Test
    public void testPhotoListAPIError() {

        Mockito.verify(activity.getGetPhotosService()).getPhotosList(Mockito.anyString(), Mockito.anyInt(), cb.capture());

        cb.getValue().onFailure(new Throwable());
        Assert.assertTrue(ShadowToast.getTextOfLatestToast().contains("Unable to fetch data from the server"));
        Assert.assertEquals(activity.getPhotosAdapter().getItemCount(), 0);
    }
}
