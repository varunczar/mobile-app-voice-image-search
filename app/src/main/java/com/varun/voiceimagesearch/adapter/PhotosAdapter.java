package com.varun.voiceimagesearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.varun.voiceimagesearch.R;
import com.varun.voiceimagesearch.core.MyApplication;
import com.varun.voiceimagesearch.model.Photo;
import com.varun.voiceimagesearch.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * This class aids in constructing individual image cells for display in the 3 column Grid
 */
public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {
    private List<Photo> mPhotoList;

    private PhotoClickListener mPhotoClickListener;
    /**
     * Setting the adapter data
     */
    public PhotosAdapter(List<Photo> photoList, PhotoClickListener photoClickListener) {
        mPhotoList = photoList;
        mPhotoClickListener = photoClickListener;
    }

    @Override
    public PhotosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new View
        View v = LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.layout_image, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        /*Setting image and title*/
        if (mPhotoList != null && position < mPhotoList.size()) {
            final Photo photo = mPhotoList.get(position);
            ImageUtils.getImageRequestManagerInstance()
                    .load(ImageUtils.getPhotoUrl(photo))
                    .into(holder.mPhoto);
            holder.mTitle.setText(photo.getTitle());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPhotoClickListener.onPhotoClicked(mPhotoList.get(position));
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mPhotoList == null ? 0 : mPhotoList.size();
    }

    /**
     * This method updates the photo data
     *
     * @param photoList
     */
    public void addPhotos(List<Photo> photoList) {
        if (mPhotoList == null) {
            mPhotoList = new ArrayList<>();
        }
        /** Calculate the size of the data */
        int position = mPhotoList.size();
        mPhotoList.addAll(photoList);
        /** Inform the adapter to update itself at the position of new data*/
        notifyItemInserted(position);
    }

    /**
     * This method clears the list data
     */
    public void resetData() {
        mPhotoList.clear();
    }

    /**
     * Creating a view holder that holds the imageview and title
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mPhoto;
        public TextView mTitle;

        public ViewHolder(View v) {
            super(v);
            mPhoto = v.findViewById(R.id.imageview_photo);
            mTitle = v.findViewById(R.id.textview_title);
        }
    }

    public interface PhotoClickListener {
        void onPhotoClicked(Photo photo);
    }
}