package com.varun.voiceimagesearch.service;

import com.varun.voiceimagesearch.model.WebResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface GetPhotosService {

    /**
     * Using Query parameters.
     */
    @GET("/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&per_page=20")
    Call<WebResponse> getPhotos(
            @Query("page") String page,
            @Query("api_key") String apiKey,
            @Query("text") String text
    );
}
