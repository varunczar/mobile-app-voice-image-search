package com.varun.voiceimagesearch.core;

import android.app.Application;
import android.content.Context;

/**
 * This is the main application class that handles initialisation of required instances at a global
 * level
 */
public class MyApplication extends Application {
    private static Application instance;

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
