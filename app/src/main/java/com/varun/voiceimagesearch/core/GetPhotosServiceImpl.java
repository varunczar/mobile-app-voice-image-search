package com.varun.voiceimagesearch.core;

import android.util.Log;

import com.varun.voiceimagesearch.R;
import com.varun.voiceimagesearch.model.WebResponse;
import com.varun.voiceimagesearch.network.RetrofitInstance;
import com.varun.voiceimagesearch.service.GetPhotosService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetPhotosServiceImpl implements MainFactory.GetPhotosInteractor {

    private static String TAG = GetPhotosServiceImpl.class.getName();

    private Call<WebResponse> mCall;

    /**
     * This method calls the flickr api and returns an instance of photos for a searchparamater and page number
     *
     * @param searchParameter    search test
     * @param page               page number
     * @param onFinishedListener a call back listener to respond with data and/or error
     */
    @Override
    public void getPhotosList(String searchParameter, int page, final OnFinishedListener onFinishedListener) {

        Log.i(TAG, "Fetching query for [" + searchParameter + "] at page [" + page + "]");

        if (mCall != null) {
            Log.i(TAG, "Cancelling unfinished calls");
            /** Cancelling any unfinished calls */
            mCall.cancel();
        }

        /** Call the method with parameter in the interface to get the photos data*/
        mCall = getPhotosService().getPhotos(String.valueOf(page),
                MyApplication.getContext().getString(R.string.api_key),
                searchParameter);

        mCall.enqueue(new Callback<WebResponse>() {
            @Override
            public void onResponse(Call<WebResponse> call, Response<WebResponse> response) {
                if (response.body() != null && response.body().getPhotosInstance() != null) {
                    Log.i(TAG, "Successful response returned [" + response + "]");
                    onFinishedListener.onFinished(response.body().getPhotosInstance());
                } else {
                    Log.i(TAG, "Empty data returned");
                    onFinishedListener.onFailure(new Throwable(MyApplication.getContext().getResources().getString(R.string.error_message_empty)));
                }

            }

            @Override
            public void onFailure(Call<WebResponse> call, Throwable t) {
                Log.i(TAG, "Unable to fetch data from the server");
                onFinishedListener.onFailure(t);
            }
        });

    }

    /**
     * This method returns a instance of the service to fetch photos data
     *
     * @return service call instance
     */
    public GetPhotosService getPhotosService() {
        return RetrofitInstance.getRetrofitInstance().create(GetPhotosService.class);
    }

}