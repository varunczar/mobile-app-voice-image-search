package com.varun.voiceimagesearch.core;

import com.varun.voiceimagesearch.model.Photo;
import com.varun.voiceimagesearch.model.Photos;

import java.util.List;

public class MainFactory {

    public interface MainController {

        void onDestroy();

        void requestDataFromServer(String searchParameter, boolean shouldReset);

        int getNextPage();

        void setNextPage(int nextPage);

        int getPages();

        void setPages(int pages);

        void resetPages();

    }

    public interface MainView {

        void showErrorMessage(String message);

        void showProgress();

        void hideProgress();

        void setPhotosData(List<Photo> photoList, boolean shouldReset);

        void onResponseFailure(Throwable throwable);


    }

    public interface GetPhotosInteractor {

        void getPhotosList(String searchParameter, int page, OnFinishedListener onFinishedListener);

        interface OnFinishedListener {
            void onFinished(Photos photos);

            void onFailure(Throwable t);
        }
    }
}
