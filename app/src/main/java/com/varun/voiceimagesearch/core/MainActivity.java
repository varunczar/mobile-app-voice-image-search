package com.varun.voiceimagesearch.core;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.varun.voiceimagesearch.R;
import com.varun.voiceimagesearch.adapter.PhotosAdapter;
import com.varun.voiceimagesearch.model.Photo;
import com.varun.voiceimagesearch.utils.GridColumnMarginsGenerator;
import com.varun.voiceimagesearch.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.varun.voiceimagesearch.utils.Constants.GRID_COLUMNS;
import static com.varun.voiceimagesearch.utils.Constants.GRID_SPACING_FACTOR;
import static com.varun.voiceimagesearch.utils.Constants.IS_ORIENTATION_CHANGE;
import static com.varun.voiceimagesearch.utils.Constants.LIST_NEXTPAGE;
import static com.varun.voiceimagesearch.utils.Constants.LIST_PAGES;
import static com.varun.voiceimagesearch.utils.Constants.LIST_PHOTOS;
import static com.varun.voiceimagesearch.utils.Constants.LIST_PHOTOS_POSITION;
import static com.varun.voiceimagesearch.utils.Constants.LIST_PHOTOS_QUERY;

/**
 * This is the main view of the application letting users interact with it
 */
public class MainActivity extends AppCompatActivity implements MainFactory.MainView, PhotosAdapter.PhotoClickListener {

    public static final String TAG = MainActivity.class.getName();
    @BindView(R.id.layout_welcome)
    LinearLayout mLayoutWelcome;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.layout_swipe)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.recyclerview_photos)
    RecyclerView mRecyclerViewPhotos;
    @BindView(R.id.progress_bar_layout)
    RelativeLayout mProgressBarLayout;
    @BindView(R.id.progress_bar_page)
    ProgressBar mProgressBarPage;

    @OnClick(R.id.fab_voice_input)
    public void onVoiceInputClicked(View view)
    {
        Toast.makeText(this,getResources().getString(R.string.to_be_implemented_message),Toast.LENGTH_SHORT).show();
    }
    /**
     * Controller instance
     */
    private MainFactory.MainController mMainController;
    /**
     * Instance to the photoservice
     */
    private GetPhotosServiceImpl mGetPhotosService;
    /**
     * List of photo instances
     */
    private List<Photo> mPhotoList;
    /**
     * The search view
     */
    private SearchView mSearchView;
    /**
     * A reference to the search menu
     */
    private MenuItem mSearchMenuItem;
    /**
     * A reference to the search query
     */
    private String mSearchQuery = "";
    private Handler mHandler;
    private boolean mIsOrientationChange = false;
    private boolean mLoading = false;
    private PhotosAdapter mPhotosAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public PhotosAdapter getPhotosAdapter() {
        return mPhotosAdapter;
    }

    public GetPhotosServiceImpl getGetPhotosService() {
        if (mGetPhotosService == null) {
            mGetPhotosService = new GetPhotosServiceImpl();
        }
        return mGetPhotosService;
    }

    public void setGetPhotosService(GetPhotosServiceImpl mGetPhotosService) {
        this.mGetPhotosService = mGetPhotosService;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        /** Initialise the main controller */
        mMainController = new MainControllerImpl(this, getGetPhotosService());
        mPhotoList = new ArrayList<>();
        mHandler = new Handler();

        /** Hide progress layout*/
        hideProgress();
        /** Show the welcome screen on first launch*/
        showWelcome();

        mLayoutManager = new GridLayoutManager(MyApplication.getContext(), GRID_COLUMNS);
        mRecyclerViewPhotos.setLayoutManager(mLayoutManager);
        mPhotosAdapter = new PhotosAdapter(mPhotoList,this );

        /** Setting up the pull down to refresh layout*/
        setupSwipeRefreshLayout();

        /** Setting up the recycler view*/
        setupRecyclerView();

        /** Handle orientation changes if needed */
        handleOrientationChanges(savedInstanceState);

    }

    /**
     * This method sets up the Swipe refresh layout
     */
    private void setupSwipeRefreshLayout() {
        /** Set refreshing to false so as to now show the refresh progress*/
        setRefreshing(false);
        /** Setting the onrefresh listener that fetches new data on refresh*/
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mMainController.resetPages();
                fetchDataFromTheServer(mSearchQuery, true);
            }
        });
    }

    /**
     * This method is responsible for handling orientation changes
     *
     * @param savedInstanceState
     */
    private void handleOrientationChanges(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            /** Set the next page reference*/
            mMainController.setNextPage(savedInstanceState.getInt(LIST_NEXTPAGE));
            /** Set the number of pages returned*/
            mMainController.setPages(savedInstanceState.getInt(LIST_PAGES));
            /** Fetch and set the photodata*/
            List<Photo> photoList = savedInstanceState.getParcelableArrayList(LIST_PHOTOS);
            setPhotosData(photoList, false);
            /** Set the recycler view scroll position*/
            int scrollPosition = savedInstanceState.getInt(LIST_PHOTOS_POSITION, 0);
            ((LinearLayoutManager) mRecyclerViewPhotos.getLayoutManager()).scrollToPositionWithOffset(scrollPosition, 0);
            /** Fetch the search query parameter*/
            mSearchQuery = savedInstanceState.getString(LIST_PHOTOS_QUERY);
            Log.i(TAG, "Query fetched " + mSearchQuery);
            /** Fetch the orientation change flag*/
            mIsOrientationChange = savedInstanceState.getBoolean(IS_ORIENTATION_CHANGE);
            /** Hide the welcome screen for an orientation change*/
            if (photoList != null && !photoList.isEmpty()) {
                hideWelcome();
            }

        }
    }

    /**
     * This method sets up the recycler view to display a 3 column image gallery from flickr
     */
    private void setupRecyclerView() {
        /** Set the adapter*/
        mRecyclerViewPhotos.setAdapter(mPhotosAdapter);
        /** Add spacing between cards*/
        mRecyclerViewPhotos.addItemDecoration(new GridColumnMarginsGenerator(GRID_SPACING_FACTOR));

        /** Add a scroll listener that fetches the next page when scrolled to the bottom of the screen*/
        mRecyclerViewPhotos.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition() ==
                        mLayoutManager.getItemCount() - 1) {
                    // The end of the recycler view is reached. Fetch the next page
                    if (!mLoading) {
                        mLoading = true;
                        showPageProgress();
                        fetchDataFromTheServer(mSearchQuery, false);
                    }
                }

                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }

    /**
     * This method sets/unsets the refreshing status of the pull to refresh layout
     *
     * @param isRefreshing
     */
    private void setRefreshing(boolean isRefreshing) {
        mSwipeRefreshLayout.setRefreshing(isRefreshing);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * This method shows the welcome screen
     */
    public void showWelcome() {
        mLayoutWelcome.setVisibility(View.VISIBLE);
    }

    /**
     * This method hides the welcome screen
     */
    public void hideWelcome() {
        mLayoutWelcome.setVisibility(View.GONE);
    }

    /**
     * This method displays the progress during the network data fetch
     */
    @Override
    public void showProgress() {
        mProgressBarLayout.setVisibility(View.VISIBLE);
    }

    /**
     * This method displays the progress at the bottom right corner of the screen during a new
     * page fetch
     */
    public void showPageProgress() {
        mProgressBarPage.setVisibility(View.VISIBLE);
    }

    /**
     * This method hides the progress UI
     */
    @Override
    public void hideProgress() {
        mProgressBarLayout.setVisibility(View.GONE);
        mProgressBarPage.setVisibility(View.GONE);
        hideWelcome();
        setRefreshing(false);
    }

    /**
     * This method sets the photodata to display
     *
     * @param photoList
     * @param shouldReset
     */
    @Override
    public void setPhotosData(List<Photo> photoList, boolean shouldReset) {

        if (shouldReset) {
            /** Reset the lists for a fresh fetch of data */
            resetData();
        }
        mLoading = false;
        /** Set the photo data */
        mPhotoList.addAll(photoList);
        mPhotosAdapter.addPhotos(photoList);

    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        /** Display an error toast message on failure */
        if (throwable != null) {
            showErrorMessage(MyApplication.getContext().getString(R.string.error_message) + throwable.getMessage());
        } else {
            showErrorMessage(MyApplication.getContext().getString(R.string.error_message));
        }
    }

    /**
     * This method displays the error message via a Toast
     *
     * @param message
     */
    public void showErrorMessage(String message) {
        Toast.makeText(MyApplication.getContext(),
                message,
                Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainController.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        mSearchMenuItem = menu.findItem(R.id.search);
        /** Initialise the searchview */

        SearchManager searchManager =
                (SearchManager) getSystemService(MyApplication.getContext().SEARCH_SERVICE);
        mSearchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        mSearchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!TextUtils.isEmpty(mSearchQuery)) {
            final String query = mSearchQuery;
            /** Set the search query if present, on orientation change*/
            if (mHandler != null
                    && mSearchView != null
                    && mSearchMenuItem != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mSearchMenuItem.expandActionView();
                        mSearchView.setQuery(query, true);
                    }
                });
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /** Do not re-submit the query on orientation change*/
        if (!mIsOrientationChange) {
            handleIntent(intent);
        } else {
            mIsOrientationChange = false;
        }
    }

    private void handleIntent(Intent intent) {

        /** If the action is search, submit a new request to the server for data*/
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.i(TAG, "Query submitted " + query);
            //mSearchView.setQuery("", false);
            mSearchMenuItem.collapseActionView();
            showProgress();
            resetData();
            mLoading = true;
            if (!TextUtils.isEmpty(query)) {
                fetchDataFromTheServer(query, true);
                mSearchQuery = query;
            }
        }
    }

    /**
     * This method calls on the controller to fetch new data from the server
     *
     * @param query
     * @param shouldReset
     */
    public void fetchDataFromTheServer(String query, boolean shouldReset) {
        Log.i(TAG, "Calling the controller to fetch data for [" + query + "]");
        mMainController.requestDataFromServer(query, shouldReset);
    }

    /**
     * This method resets page counter and photos data
     */
    private void resetData() {
        mMainController.resetPages();
        mPhotoList.clear();
        mPhotosAdapter.resetData();
    }

    /**
     * This method is called before the application is destroyed enabling us to save it's current
     * state if an orientation change handling is needed
     *
     * @param bundle
     */
    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        /** Store the current scroll position of the recyclerview*/
        int lastFirstVisiblePosition = ((LinearLayoutManager) mRecyclerViewPhotos.getLayoutManager())
                .findFirstCompletelyVisibleItemPosition();
        bundle.putInt(LIST_PHOTOS_POSITION, lastFirstVisiblePosition);
        /** Store the number of pages returned*/
        bundle.putInt(LIST_PAGES, mMainController.getPages());
        /** Store the next page reference*/
        bundle.putInt(LIST_NEXTPAGE, mMainController.getNextPage());
        /** Store the search query parameter*/
        if (mSearchView.getQuery() != null && !TextUtils.isEmpty(mSearchView.getQuery().toString())) {
            bundle.putString(LIST_PHOTOS_QUERY, mSearchView.getQuery().toString());
        } else if (!TextUtils.isEmpty(mSearchQuery)) {
            bundle.putString(LIST_PHOTOS_QUERY, mSearchQuery);
        }
        /** Store the photos data*/
        bundle.putParcelableArrayList(LIST_PHOTOS, new ArrayList<>(mPhotoList));
        /** Store the flag stating that the orientation is changing*/
        bundle.putBoolean(IS_ORIENTATION_CHANGE, true);
    }


    @Override
    public void onPhotoClicked(Photo photo)
    {
        /** Creating a dialog to display the clicked photo*/
        //TODO Possibly move this to a utility class?
        Dialog builder = new Dialog(this,android.R.style.Theme_Black);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor(getResources().getString(R.string.black_background_string))));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });
        String imageUrl = ImageUtils.getPhotoUrl(photo);
        Log.i(TAG,"Displaying image with URL "+imageUrl);
        ImageView imageView = new ImageView(this);
        ImageUtils.getImageRequestManagerInstance().
                load(imageUrl).into(imageView);
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }
}
