package com.varun.voiceimagesearch.core;

import android.util.Log;

import com.varun.voiceimagesearch.R;
import com.varun.voiceimagesearch.model.Photos;

/**
 * This controller class requests data from the model and presents it to the view for displaying
 */
public class MainControllerImpl implements MainFactory.MainController, MainFactory.GetPhotosInteractor.OnFinishedListener {

    private static final String TAG = MainControllerImpl.class.getName();
    /**
     * View
     */
    private MainFactory.MainView mMainView;
    /**
     * Model
     */
    private MainFactory.GetPhotosInteractor mGetPhotosInteractor;
    /**
     * Reference to the next page
     */
    private int nextPage = 1;
    /**
     * Total number of pages
     */
    private int pages = 1;
    /**
     * Flag that determines when the data should be reset
     */
    private boolean mShouldReset = false;

    /**
     * This constructor sets up the view and the model
     *
     * @param mainView
     * @param getPhotosInteractor
     */
    public MainControllerImpl(MainFactory.MainView mainView, MainFactory.GetPhotosInteractor getPhotosInteractor) {
        this.mMainView = mainView;
        this.mGetPhotosInteractor = getPhotosInteractor;
    }

    @Override
    public int getNextPage() {
        return nextPage;
    }

    @Override
    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    @Override
    public int getPages() {
        return pages;
    }

    @Override
    public void setPages(int pages) {
        this.pages = pages;
    }

    /**
     * Reset page numbers
     */
    @Override
    public void resetPages() {
        this.nextPage = 1;
        this.pages = 1;
    }

    @Override
    public void onDestroy() {
        /** Destroying instances of view and model */
        this.mMainView = null;
        this.mGetPhotosInteractor = null;

    }

    /**
     * This methods calls on the model to fetch photos for a searchParameter
     *
     * @param searchParameter
     * @param shouldReset
     */
    @Override
    public void requestDataFromServer(String searchParameter, boolean shouldReset) {

        Log.i(TAG, "Requesting data from the server for [" + searchParameter + "]");
        Log.i(TAG, "Reset flag is [" + shouldReset + "]");
        Log.i(TAG, "Next page is  [" + nextPage + "]");
        Log.i(TAG, "Number of pages are  [" + pages + "]");
        mShouldReset = shouldReset;
        /** Call the server for data if there are more pages to be fetched */
        if (nextPage <= pages) {
            mGetPhotosInteractor.getPhotosList(searchParameter, nextPage, this);
        }
    }

    @Override
    public void onFinished(Photos photos) {
        if (mMainView != null) {
            if (photos != null && photos.getPhotos() != null && !photos.getPhotos().isEmpty()) {
                /** Send the successful fetch of data to the view to render*/
                Log.i(TAG, "Sending successful data to the view");
                mMainView.setPhotosData(photos.getPhotos(), mShouldReset);
                pages = photos.getPages();
                nextPage = photos.getPage() + 1;
            } else {
                /** Send an errormessage to the view to display*/
                Log.i(TAG, "Sending error to the view for lack of data");

                mMainView.showErrorMessage(MyApplication.getContext().getResources().getString(R.string.error_message_empty));
            }
            mMainView.hideProgress();
        }
    }

    public void onFailure(Throwable t) {
        /** Send an errormessage to the view to display*/
        Log.i(TAG, "Sending error to the view for failure");
        if (mMainView != null) {
            mMainView.onResponseFailure(t);
            mMainView.hideProgress();
        }

    }


}