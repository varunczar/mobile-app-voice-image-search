package com.varun.voiceimagesearch.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * This utility class assists in creating spaces between cards in the grid
 */
public class GridColumnMarginsGenerator extends RecyclerView.ItemDecoration {

    private int mMargin;

    public GridColumnMarginsGenerator(int margin) {
        this.mMargin = margin;
    }

    @Override
    public void getItemOffsets(Rect rect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        rect.left = mMargin;
        rect.right = mMargin;
        rect.bottom = mMargin;
        rect.top = mMargin;
    }
}
