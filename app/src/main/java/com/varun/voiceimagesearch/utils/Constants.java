package com.varun.voiceimagesearch.utils;

public class Constants {

    public static final int GRID_COLUMNS = 3;
    public static final int GRID_SPACING_FACTOR = 10;
    public static final String IS_ORIENTATION_CHANGE = "orientation_change";
    public static final String LIST_NEXTPAGE = "next_page";
    public static final String LIST_PAGES = "pages";
    public static final String LIST_PHOTOS = "list_photos";
    public static final String LIST_PHOTOS_POSITION = "list_photos_position";
    public static final String LIST_PHOTOS_QUERY = "list_photos_query";
}
