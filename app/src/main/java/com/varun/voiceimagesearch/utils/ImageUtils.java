package com.varun.voiceimagesearch.utils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.varun.voiceimagesearch.R;
import com.varun.voiceimagesearch.core.MyApplication;
import com.varun.voiceimagesearch.model.Photo;

/**
 * This utility class assists in managing image display and creation of image urls
 */
public class ImageUtils {

    /**
     * Base url for an image
     */
    private static final String IMAGE_URL = "http://farm%1$s.static.flickr.com/%2$s/%3$s_%4$s.jpg";
    /**
     * Config for a glide instance
     */
    private static RequestManager mRequestManager;

    /**
     * This method generates an image url using an instance of photo
     *
     * @param photo
     * @return
     */
    public static String getPhotoUrl(Photo photo) {
        if (photo == null) {
            return "";
        }
        /** Create an instance of an image url using the farm, server, id and secret of */
        return String.format(IMAGE_URL, photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret());
    }


    /**
     * This method returns a Glide instance for fetching and displaying image data
     *
     * @return pre-configured Glide instance
     */
    public static RequestManager getImageRequestManagerInstance() {
        if (mRequestManager == null) {
            RequestOptions myOptions = new RequestOptions()
                    /** Setting the placeholder image */
                    .placeholder(R.drawable.ic_launcher_background)
                    /** Setting an image for when downloading fails */
                    .error(R.drawable.ic_launcher_background)
                    /** Setting diskcache strategy to all */
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            /** Create an instance of Glide*/
            mRequestManager = Glide.with(MyApplication.getContext()).
                    applyDefaultRequestOptions(myOptions);
        }
        return mRequestManager;
    }
}
